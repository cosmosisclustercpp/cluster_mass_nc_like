#!/usr/bin/env python
import numpy as np

ln2pi = np.log(2*np.pi)

class LikeFuncs():
    '''
    Object with likelihood functions

    Attributes
    ----------
    debug: bool
        Print diagnostics tests
    '''

    def __init__(self, debug=False):
        '''
        Parameters
        ----------
        debug: bool
            Print diagnostics tests
        '''
        self.debug = debug
    def glue_cov(self, cov1, cov2):
        '''
        Joins two covariance matrices assuming no correlation

        Parameters
        ----------
        cov1: 2d array
            Covariance matrix (n1, n1) format
        cov2: 2d array
            Covariance matrix (n2, n2) format

        Returns
        -------
        2d array
            Covariances matrices joined, (n1+n2, n1+n2) format
        '''
        cov_off = np.zeros((len(cov1), len(cov2)))
        cova = np.vstack([cov1, cov_off.T])
        covb = np.vstack([cov_off, cov2])
        #print cova.shape, covb.shape
        return np.vstack([cova.T, covb.T])
    def ln_gauss_prior(self, val, mean, sig):
        '''
        Computes ln of gaussian likelihood for scalar parameter

        Parameters
        ----------
        val: float
            Value of variable
        mean: float
            Mean value for gaussian
        sig: float
            Scatter for gaussian

        Returns
        -------
        float
            ln of gaussian likelihood
        '''
        return - .5 * (mean-val)**2 / sig**2 - .5 * ln2pi - np.log(sig)
    def lnprob(self, obs, teo, cov, det=True, norm=True):
        '''
        Computes ln of gaussian likelihood for vector

        Parameters
        ----------
        obs: numpy array
            Observed data vector
        teo: numpy array
            Theoretical prediction of data vector
        cov: numpy array (2d)
            Covariance of data
        det:
            Add det(cov) to likelihood
        norm:
            Add 1/2*log(2pi**ndim) to likelihood

        Returns
        -------
        lnlk: float
            ln of gaussian likelihood
        '''
        icov = np.linalg.inv(cov)
        diff = (teo-obs)

        # a block to allow easy comparison of matteo's theory prediction
        # and our theory prediction. Change the corresponding
        # if_reproduce_mtheory to True also in likelihood.py
        if_reproduce_mtheory = False
        if if_reproduce_mtheory:
            print("ours, matteos, difference")
            print(teo)
            print(obs)
            print(diff/teo)

        lnlk = -.5*np.dot( np.dot(diff, icov), diff ) 
        if det:
            lnlk += -.5*np.log(np.linalg.det(cov))
        if norm:
            lnlk += -.5*ln2pi*len(diff)
        if self.debug:
            identity = np.dot(cov, icov)
            print('Test for likelihood:')
            print(' - max deviation diagonal: %s'%str(
                    max(np.diag(identity)-1)))
            print(' - max deviation off diagonal: %s'%str(
                    np.amax(identity-np.diag(np.diag(identity)))))
            print(' - ln(det(cov)):%s'%np.log(np.linalg.det(cov)))
        return lnlk
