from cosmosis.datablock import names, option_section
from funcs_like import LikeFuncs
from matteo_data import get_data
import numpy as np

# We have a collection of commonly used pre-defined block section names.
# If none of the names here is relevant for your calculation you can use any
# string you want instead.
likes = names.likelihoods
cluster_abundance = 'cluster_abundance'

def setup(options):
    '''
    Notes
    -----
    '''
    #This function is called once per processor per chain.
    #It is a chance to read any fixed options from the configuration file,
    #load any data, or do any calculations that are fixed once.

    #Use this syntax to get a single parameter from the ini file section
    #for this module.  There is no type checking here - you get whatever the user
    #put in.

    config = {}

    config['nui_pars'] = []
    config['cosm_pars'] = []

    for t in ['nui', 'cosm']:
        config['%s_fid'%t] = [ options[option_section, p] for p in config['%s_pars'%t]]
        config['%s_sig'%t] = [ options[option_section, 'sig_%s'%p] for p in config['%s_pars'%t]]
    #config['mass_cov'] = options["<<some section>>", 'covariance']

# <<temp part>>
    config['cov_mass'] = get_data()[2][5:].T[5:]
# <<end temp part>>

    config['debug'] = False
    if options.has_value(option_section, "debug"):
        if isinstance(options[option_section, "debug"],  bool):
            config['debug'] = options[option_section, "debug"]
    config['LikeFuncs'] = LikeFuncs(config['debug'])

    #Now you have the input options you can do any useful preparation
    #you want.  Maybe load some data, or do a one-off calculation.

    #Whatever you return here will be saved by the system and the function below
    #will get it back.  You could return 0 if you won't need anything.
    return config


def execute(block, config):
    '''
    Computers the likelihood for cluster number counts and masses

    Notes
    -----
    Temporary:
        Covariance matrix of mass used is from measurements
        Masses are not being considered for likelihood computation
    '''
    #This function is called every time you have a new sample of cosmological and other parameters.
    #It is the main workhorse of the code. The block contains the parameters and results of any
    #earlier modules, and the config is what we loaded earlier.

    # Just a simple rename for clarity.
    #obs = config['data']

    # Let's get the pipeline calculation: the masses are stored as radial profiles
    teo_counts = block[cluster_abundance, 'centered_cluster_counts']
    teo_mass = block[cluster_abundance, 'centered_gamma_ts']
    omega_m = block["cosmological_parameters", "omega_m"] # needed for mass cosmology correction

# <<temp part>>
    # Let's get the SDSS cosmology from Matteo- there is both data and theory in the file
    matteo_teo, obs, cov, data_logm_30 = get_data()
    if_reproduce_mtheory = False
    if if_reproduce_mtheory :
        obs = matteo_teo  # let us compare matteo's prediction with ours,
                          #  as opposed to comparing matteo's data with ours

    # now, the masses are encoded into the gamma_t radial profiles.
    # We want just one mass per z,lambda bin
    n_lam_bins=5; n_z_bins=1; n_radii=10
    ix = np.arange(0,n_radii*n_lam_bins*n_z_bins, n_radii)
    teo_mass= np.log10(teo_mass[ix]/teo_counts)

    # we need to deal with the cosmology dependence of the weak lensing masses
    # essentially, they measured Delta Sigma instead of gamma_t
    # From Matteo's matteo_data.py
# To correct for Omega_m the data use:
# logM(Omega_m)=logM(Omega_m=0.30)+dlogMdOmegam*(Omega_m-0.300)
#dlogMdOmegam = np.array([-0.64703478, -0.66181482, -0.67657603, -0.77281915, -0.65220622])
#data_logm_18 = list(data_logm_30+ dlogMdOmegam *(1.87518978e-01-.3) )
    dlogMdOmegam = np.array([-0.64703478, -0.66181482, -0.67657603, -0.77281915, -0.65220622])
    obs_mass = data_logm_30 + dlogMdOmegam * (omega_m-0.300)
    # now overwrite the data vector masses with our corrected data masses
    obs[5:] = obs_mass

    # and create the data vector, counts + masses
    teo= np.append(teo_counts,teo_mass)
# <<end temp part>>

    #cov_nc = block[cluster_abundance, 'covariance']
    cov_mass = config['cov_mass']
    #cov = funcs_like.glue_cov(cov_nc, cov_mass)

    params = {}
    lnlike = 0
    for p in config['nui_pars'] + config['cosm_pars']:
        if '%s_sig'%p in config:
            lnlike += config['LikeFuncs'].ln_gauss_prior(
                                            block[option_section, p],
                                            config['%s_fid'%p],
                                            config['%s_sig'%p])
    lnlike += config['LikeFuncs'].lnprob(obs, teo, cov, True, False)
    if config['debug']:
        #print(-lnlike/6.7622073612037310 - 1.)
        print('Likelihood input:')
        print(' - Prediciton: %s'%str(teo))
        print(' - Data: %s'%str(obs))
        print(' - Covariance: %s'%str(cov))
        print('ln(likelihood): %s'%str(lnlike))
        print()
    block[likes,'like_cluster_mass_nc_like'] = lnlike
    #We tell CosmoSIS that everything went fine by returning zero
    return 0

def cleanup(config):
    # Usually python modules do not need to do anything here.
    # We just leave it in out of pedantic completeness.
    pass
